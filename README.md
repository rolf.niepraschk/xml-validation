# xml-validation

REST service for the validation of ```xml``` against a XML Schema (```xsd```).

## install & activate

Create a file `.git/hooks/pre-push` inside the GIT repository:

```
cd .git/hooks/
ln -s ln -sf ../../pre-push
```

Do the following steps with root permission on the server installation:

```shell
mkdir -p /usr/local/share/xml-validation
cd /usr/local/share/xml-validation
curl --silent --output - \
  https://gitlab1.ptb.de/niepra01/xml-validation/-/archive/master/xml-validation-master.tar \
  | tar xf - --strip-components=1           # only once
python3 -m venv .
source bin/activate
pip3 install pip-tools
pip-compile
chown -R nobody.nobody ../xml-validation
systemctl daemon-reload                     # if already exist
systemctl link $PWD/xml-validation.service
systemctl enable xml-validation.service     # make permanent
systemctl start xml-validation.service      
systemctl status xml-validation.service     # running?
journalctl -f --unit xml-validation.service # run-time check
```

## Configuration

File `config.json`:
```json
{
  "server":{
    "host":"0.0.0.0",
    "port": 5005
  },
  "xsd":{
    "releases":"https://www.ptb.de/dcc/releases.json"
  }
}
``` 

## Force a new version

```
./newRevision 'x.y.z' 'comment'
```

A new tag value (program version) with an optional comment is assigned to the 
current branch. This leads to an update of the `VERSION` file and, if a webhook 
(tag push) is activated, to an update of the server installation. The Flask 
server must run in development mode for this.

## API

### /validate endpoint [POST]

```shell
curl -X POST -H "Content-Type: text/xml" -d @VALID.xml http://localhost:5005/validate

<ok/>
```

Validation against an explizit schema version
```shell
curl -X POST -H "Content-Type: text/xml" -d @VALID.xml http://localhost:5005/validate?v=2.4.0

<ok/>
```

Alternative call with `wget`:
```shell
wget --post-file=example.xml --header="Content-Type:text/xml" -q -O- http://localhost:5005/validate 
```

```shell
curl -X POST -H "Content-Type: text/xml" -d @INVALID.xml http://localhost:5005/validate

<error>invalid xml data</error>
```

```shell
curl -X POST -H "Content-Type: text/xml" -d @VALID_BUT_DONT_MATCHES_XSD.xml http://localhost:5005/validate

<error>verbose error message</error>
```

### /version [GET]

Returns the version of the validation server:

```shell
curl http://localhost:5005/version

<version>0.2.0</version>
```

### /update [POST]

This URL can be used as a webhook for GitLab (Tag Push Events). With this, 
GitLab notifies the Flask server of the existence of a new program version, 
which is then requested as a tarball and unpacked.

## Web interface

```
http://localhost:5005/validation.html
```

Uses the informations from https://www.ptb.de/dcc/releases.json

A functioning PTB-internal server can be reached at the address: http://a73434.berlin.ptb.de:5005/validation.html



