from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
import utils as utils
from trans import Trans
from datetime import datetime
import subprocess, json
from pprint import pprint

config = utils.get_config_dict()
        
app = Flask(__name__)
CORS(app)
trans = Trans()

@app.route('/version', methods=['GET'])
def version():
    app.logger.debug('hit version')
    version = utils.get_version()
    if version:
        return jsonify({'version':version})
    else:
        return jsonify({'error':'Version unknown'})
        
@app.route('/update', methods=['POST'])
def update():
    app.logger.debug('hit update')
    req = request.get_json()
    ###pprint(req)
    tarball_url = req['repository']['homepage'] + '/-/archive/master/' + \
      req['repository']['name'] + '-master.tar'
    version = str(utils.get_version())
    with open('./LOG', 'a') as f:    
        print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] " + version), file=f)
    try:
        ps = subprocess.Popen(('/usr/bin/curl', '--insecure', '--silent', \
          '--output', '-', tarball_url), stdout=subprocess.PIPE)
        output = subprocess.check_output(('/usr/bin/tar', '--extract', \
          '-f', '-', '--strip-components=1'), stdin=ps.stdout)
        ps.wait()
    except Exception as error:
        app.logger.debug(str(error)) 
        return jsonify({'error':str(error)})
        
    return jsonify({'OK':True})
        
@app.route('/validate', methods=['POST'])
def validate():
    app.logger.debug('hit validate')
    version = request.args.get('v')
    xml_str = request.data
    xml_tree = utils.parse(xml_str)
    releases_dict = utils.get_releases_dict(config['xsd']['releases'])
    try:
        utils.parse(xml_str)
        app.logger.debug('=== xml_tree OK ===')
        xsd_str = utils.get_xsd(releases_dict, xsd_version=version, xml_str=xml_str)
        if  xsd_str:
            app.logger.debug('=== xsd OK ===')
            ret = utils.validate(xml_str=xml_str, xsd_str=xsd_str)
        else:
            app.logger.debug('=== xsd invalid ===')
            ret = utils.return_error(error='xsd content not readable')
    except:
        app.logger.debug('=== xml_tree invalid ===')
        ret = utils.return_error(error='invalid xml data')   

    return  utils.xml_response(ret)
    
@app.route('/validation.html', methods=['GET'])
def validation():
    app.logger.debug('hit validation.html')
    releases_dict = utils.get_releases_dict(config['xsd']['releases'])
    versions = utils.get_versions(releases_dict) 
    l = str(request.accept_languages).split(',')[0][0:2]
    x = '[' + ','.join(versions) + ']'# javascript array of versions 
    return trans.show_html(version=utils.get_version(), language=l, 
      xsd_versions=x)

@app.route('/js/<fn>', methods=['GET'])
def js_folder(fn):
    app.logger.debug('hit js folder')
    return send_from_directory('static/js', fn)

@app.route('/css/<fn>', methods=['GET'])
def css_folder(fn):
    app.logger.debug('hit css folder')
    return send_from_directory('static/css', fn)

@app.route('/img/<fn>', methods=['GET'])
def logo_folder(fn):
    app.logger.debug('hit logo folder')
    return send_from_directory('static/img', fn)

if __name__ == '__main__':
    app.run(host=config['server']['host'], port=config['server']['port'])


