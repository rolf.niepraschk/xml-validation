from flask import Flask, render_template

class Trans:
    mapping = {
      'de':{
        'TITEL': '-Validierungsservice',
        'UPLOAD': 'DCC-XML-Datei hochladen',
        'FILE': 'Datei',
        'XSD_VERSION': 'XSD-Version',
        'INTERNAL': 'XML-intern',
        'LATEST': 'neueste'
      },
      'en':{
        'TITEL': ' Validation Service',
        'UPLOAD': 'Upload the DCC XML file',
        'FILE': 'File',
        'XSD_VERSION': 'XSD version',
        'INTERNAL': 'inside XML',
        'LATEST': 'latest'
      },
      'ru':{
        'TITEL': ' Служба валидации',
        'UPLOAD': 'Загрузить файл DCC XML',
        'FILE': 'файл',
        'XSD_VERSION': 'XSD версия',
        'INTERNAL': 'внутри XML',
        'LATEST': 'последний'
      }
    }
    def show_html(self, version, language, xsd_versions):
        d = self.mapping[language]
        if not d:
            d = self.mapping['en']
        template = 'html/validation.html'
        return render_template(template, VERSION=version, 
          XSD_VERSIONS=xsd_versions, LANGUAGE=language, **d)
