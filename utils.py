import json
import requests
from flask import Response
from xml.etree import ElementTree as ET
import xmlschema
import sys
from urllib.parse import urlparse
#from pprint import pprint

# https://stackoverflow.com/questions/7160737/python-how-to-validate-a-url-in-python-malformed-or-not
def uri_validator(x):
    try:
        result = urlparse(x)
        return all([result.scheme, result.netloc, result.path])
    except:
        return False

def get_config_dict():
    with open('./config.json') as json_config_file:
        config = json.load(json_config_file)

    return config

# get content of 'releases.json' via http request
def get_releases_dict(url):
    try:
        filename = urlparse(url).path.split('/')[-1]
        r = requests.get(url, allow_redirects=True)
        if r.url.endswith(filename): # no bad redirection
            try:
                return json.loads(r.text)
            except:
                return False         
        else:
            return False
    except:
        return False  

# a reverse list of version strings        
def get_versions(d):
    v = []
    for i in d['releases']:
        v.append('"' + i['version'] + '"') 
    v = list(reversed(v))
    return v

def get_version():
    try:
        with open('./VERSION', 'r') as f:
            version = f.read().rstrip()
        return version
    except:
        return False

def get_xsd(d, xsd_version=None, xml_str = None):
    url = ''
    version = xsd_version
    if not version: # We use the internal version information in the XML data
        root = parse(xml_str)
        version = root.attrib.get('schemaVersion')
    for i in d['releases']:
        if i['version'] == version:
            url = i['url']
            break
    try:
        r = requests.get(url, allow_redirects=True)
        return r.text
    except:
        return False

def parse(xml_str):
    try:
        tree = ET.fromstring(xml_str)
    except ET.ParseError:
        tree = None
    return tree

def validate(xml_str, xsd_str):
    try:
        tree = parse(xml_str)
        schema = xmlschema.XMLSchema(xsd_str)
        schema.validate(tree)
        return return_ok() 
    except Exception as error:
        return return_error(error)
   
def xml_response(xml_str):
    return Response(xml_str, content_type='text/xml; charset=utf-8')

def return_ok():
    return '<ok/>\n'

def return_error(error):
    return '<error>{error}</error>\n'.format(error=error)

def return_version(version):
    return '<version>{version}</version>\n'.format(version=version)
